from django.core.management.base import BaseCommand
from django.utils import timezone

import factory
import factory.fuzzy
from factory.django import DjangoModelFactory

from app.models import Person, Task, Status, Commentary

import random


# class StatusFactory(DjangoModelFactory):
#     class Meta:
#         model = Status
#         django_get_or_create = ("name",)
#
#     name = factory.fuzzy.FuzzyChoice(["En proceso", "Completada", "Archivada"])


class StatusFactory(DjangoModelFactory):
    class Meta:
        model = Status

    name = factory.Faker("first_name")


class PersonFactory(DjangoModelFactory):
    class Meta:
        model = Person

    name = factory.Faker("first_name")


class TaskFactory(DjangoModelFactory):
    class Meta:
        model = Task

    owner = factory.SubFactory(PersonFactory)
    title = factory.Faker("sentence", nb_words=5, variable_nb_words=True)
    description = factory.Faker("sentence", nb_words=5, variable_nb_words=True)
    status = factory.SubFactory(StatusFactory)


class CommentaryFactory(DjangoModelFactory):
    class Meta:
        model = Commentary

    description = factory.Faker("sentence", nb_words=8, variable_nb_words=True)
    owner = factory.SubFactory(PersonFactory)
    task = factory.SubFactory(TaskFactory)


class Command(BaseCommand):
    help = "Display current time"

    def handle(self, *args, **options):
        NUMBER_TASKS = 200
        NUMBER_COMMENTS = 1500
        NUMBER_PERSON = 75
        estados = ["En proceso", "Completada", "Archivada"]

        status = []
        for estado in estados:
            e = StatusFactory(name=estado)
            status.append(e)

        self.stdout.write(f"Creando owners ....")
        # CREANDO PERSONAS
        people = []
        for _ in range(NUMBER_PERSON):
            p = PersonFactory()
            people.append(p)
        self.stdout.write(f"{NUMBER_PERSON} owners creados")

        self.stdout.write(f"Creando tareas ....")
        # CREANDO TAREAS
        tareas = []
        for _ in range(NUMBER_TASKS):
            t = TaskFactory(
                owner=random.choice(people), status=random.choice(status)
            )
            tareas.append(t)
        self.stdout.write(f"{NUMBER_TASKS} tareas creadas")

        self.stdout.write(f"Creando comentarios ....")
        # CREANDO COMENTARIOS
        for _ in range(NUMBER_COMMENTS):
            c = CommentaryFactory(
                owner=random.choice(people), task=random.choice(tareas)
            )
        self.stdout.write(f"{NUMBER_COMMENTS} tareas creadas")

        self.stdout.write(f"Hora es: ")
