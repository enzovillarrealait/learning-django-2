from django.db import models

# Create your models here.


class Status(models.Model):
    name = models.CharField(max_length=128)


class Person(models.Model):
    name = models.CharField(max_length=128)


class Task(models.Model):
    owner = models.ForeignKey(
        Person, on_delete=models.DO_NOTHING, related_name="tasks"
    )
    title = models.CharField(max_length=128, null=True)
    description = models.TextField(null=True)
    status = models.ForeignKey(
        Status, on_delete=models.DO_NOTHING, null=True, related_name="tasks"
    )


class Commentary(models.Model):
    description = models.TextField()
    task = models.ForeignKey(
        Task, on_delete=models.DO_NOTHING, related_name="comments"
    )
    owner = models.ForeignKey(
        Person, on_delete=models.DO_NOTHING, related_name="commentary_task"
    )
