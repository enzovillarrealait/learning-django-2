from rest_framework.response import Response
from rest_framework.views import APIView

from app.status.selectors import list_status
from app.status.serializers import StatusSerializer


class StatusListApi(APIView):
    def get(self, request):

        status = list_status()
        return Response(StatusSerializer(status, many=True).data, status=200)
