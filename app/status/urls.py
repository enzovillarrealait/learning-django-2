from django.urls import path

from app.status.views import StatusListApi

urlpatterns = [path("list/", StatusListApi.as_view())]
