from app.models import Person


def create_person(*, name: str):
    return Person.objects.create(name=name)
