from django.urls import path

from app.persons.views import PersonListApi, PersonCreateApi

urlpatterns = [
    path("list/", PersonListApi.as_view()),
    path("create/", PersonCreateApi.as_view()),
]
