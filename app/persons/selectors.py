from django.db.models import Q

from app.models import Person


def list_persons():
    return Person.objects.all()


def filter_persons(*, name=""):
    persons = list_persons()
    return persons.filter(Q(name__icontains=name))
