from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers

from app.persons.selectors import filter_persons
from app.persons.serializers import PersonSerializer
from app.persons.services import create_person
from app.utils.response import get_parsed_query_params


class PersonListApi(APIView):
    def get(self, request):
        persons = filter_persons(
            **get_parsed_query_params(data=request.query_params)
        )
        return Response(PersonSerializer(persons, many=True).data, status=200)


class PersonCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=128)

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        person = create_person(**serializer.validated_data)
        return Response(PersonSerializer(person).data, status=201)
