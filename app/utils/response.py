def get_parsed_query_params(data=None):
    if data is None:
        return {}
    dicc = {
        key.replace("[]", ""): data.get(key)
        if not "[]" in key
        else data.getlist(key)
        for key in data
    }
    return dicc
