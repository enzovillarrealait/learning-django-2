from django.urls import path, include

from app.tasks.views import (
    TaskListApi,
    TaskCreateApi,
    TaskUpdateStatusApi,
    TaskApi,
    TaskCommentByPersonApi,
)

urlpatterns = [
    path("get/<task_id>/", TaskApi.as_view()),
    path("list/", TaskListApi.as_view()),
    path("list/owner-comments/", TaskCommentByPersonApi.as_view()),
    path("update/state/", TaskUpdateStatusApi.as_view()),
    path("create/", TaskCreateApi.as_view()),
    path("comments/", include("app.comments.urls")),
]
