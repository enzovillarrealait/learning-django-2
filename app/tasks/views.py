from django.db.models import Sum, Count, Case, When

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers

from app.status.serializers import StatusSerializer
from app.tasks.selectors import list_tasks, get_task
from app.tasks.services import create_task, update_state_task
from app.tasks.serializers import TaskSerializer


class TaskListApi(APIView):
    def get(self, request):
        tasks = list_tasks()
        tasks = tasks.annotate(comments_count=Count("comments"))
        return Response(TaskSerializer(tasks, many=True).data, status=200)


class TaskCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        title = serializers.CharField(max_length=128)
        description = serializers.CharField()
        owner_id = serializers.IntegerField()

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        task = create_task(**serializer.validated_data)

        return Response(TaskSerializer(task).data, status=201)


class TaskApi(APIView):
    def get(self, request, task_id):
        task = get_task(task_id)
        return Response(TaskSerializer(task).data, status=200)


class TaskCommentByPersonApi(APIView):
    def get(self, request):
        tasks = (
            list_tasks()
            .filter(comments__owner_id=447)
            .annotate(
                comments_count=Count(
                    Case(When(comments__owner_id=447, then=1), default=0)
                )
            )
        )

        return Response(
            TaskSerializer(tasks, many=True, context={"owner_id": 447}).data,
            status=200,
        )


## PRUEBA
class TaskUpdateStatusApi(APIView):
    class InputSerializers(serializers.Serializer):
        id_task = serializers.IntegerField()
        id_state = serializers.IntegerField()

    def put(self, request):
        serializer = self.InputSerializers(data=request.data)
        serializer.is_valid(raise_exception=True)

        updated_state = update_state_task(**serializer.validated_data)

        return Response(StatusSerializer(updated_state).data, status=201)
