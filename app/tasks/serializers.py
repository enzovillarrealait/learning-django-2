from django.db.models import Count
from rest_framework.serializers import ModelSerializer, SerializerMethodField

from app.models import Task, Commentary
from app.persons.serializers import PersonSerializer
from app.status.serializers import StatusSerializer


class CommentarySerializer(ModelSerializer):
    owner = PersonSerializer()

    class Meta:
        model = Commentary
        fields = "__all__"


class TaskSerializer(ModelSerializer):
    status = StatusSerializer()
    owner = PersonSerializer()
    comments = CommentarySerializer(many=True)
    comments_count = SerializerMethodField()
    comments_by_owner = SerializerMethodField()

    class Meta:
        model = Task
        fields = "__all__"

    # PROBANDO SERIALIZER CON ANNOTATE
    def get_comments_count(self, obj):
        try:
            return obj.comments_count
        except:
            # SI  NO TIENE DEVUELVE COUNT
            return obj.comments.count()  # Se pode usar solo este

    def get_comments_by_owner(self, obj):
        owner_id = self.context.get("owner_id")
        if owner_id:
            return CommentarySerializer(
                obj.comments.filter(owner_id=owner_id), many=True
            ).data
        else:
            return False
