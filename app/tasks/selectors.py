from app.models import Task


def list_tasks():
    return Task.objects.all()


def get_task(task_id):
    return Task.objects.get(pk=task_id)
