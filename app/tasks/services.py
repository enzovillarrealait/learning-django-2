from app.models import Status, Task


def create_task(*, title: str, description: str, owner_id: id):
    initial_state = Status.objects.get(name="En proceso")
    return Task.objects.create(
        title=title,
        description=description,
        owner_id=owner_id,
        status_id=initial_state.pk,
    )


def update_state_task(*, id_task: int, id_state: int):
    try:
        task = Task.objects.get(pk=id_task)
        task.status_id = Status.objects.get(pk=id_state).pk
        task.save()
        return task.status
    except Exception as error:
        return error
