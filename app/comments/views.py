from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers

from app.comments.services import create_commentary
from app.comments.serializers import CommentarySerializer


class CommentaryCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        owner_id = serializers.IntegerField()
        description = serializers.CharField()

    def post(self, request, task_id):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        commentary = create_commentary(
            **serializer.validated_data, task_id=task_id
        )

        return Response(CommentarySerializer(commentary).data, status=201)
