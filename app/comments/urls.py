from django.urls import path

from app.comments.views import CommentaryCreateApi

urlpatterns = [
    path("create/<task_id>/", CommentaryCreateApi.as_view()),
]
