from app.models import Commentary


def create_commentary(*, owner_id: int, description: str, task_id: int):
    return Commentary.objects.create(
        owner_id=owner_id, description=description, task_id=task_id
    )
